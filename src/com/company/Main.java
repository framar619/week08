package com.company;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

        public static void main(String[] args) {
        ExecutorService myService = Executors.newFixedThreadPool(3);
        myService.execute(new Runnable() {
            public void run() {
                try {
                    for (int i = 1; i <= 10; i++) {
                        if (!isNumeric(String.valueOf(i))){
                            throw new Exception("Exception message");
                        }
                        System.out.println(i);
                        System.out.println("I need to run my new thread");
                    }

                } catch (Exception e) {
                    System.out.println("We need a numeric value in this thread");
                }

            }
        });

        myService.execute(new Runnable() {

            public void run() {
                try {
                    for (int i = 11; i <= 20; i++) {
                        if (!isNumeric(String.valueOf(i))){
                            throw new Exception("Exception message");
                        }
                        System.out.println(i);
                        System.out.println("I am learning about threads and runnables");
                    }

                } catch (Exception e) {
                    System.out.println("We need a numeric value in this thread");
                }

            }
        });

        Thread t3 = new Thread(new Runnable() {
            public void run() {
                for (int i = 21; i <= 30; i++) {
                    System.out.println(i);
                    System.out.println("I hope this works");
                }
            }
        });
        t3.start();

    }

    public static boolean isNumeric (String a){
        try {
            int d = Integer.parseInt(a);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;

    }

}
